package org.gcube.data.access.storagehub.fs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.file.Files;

import org.gcube.common.storagehub.client.StreamDescriptor;
import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.model.items.AbstractFileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jnr.ffi.Pointer;
import ru.serce.jnrfuse.ErrorCodes;
import ru.serce.jnrfuse.struct.FileStat;

public class FileDownload implements SHFile{

	public static Logger logger = LoggerFactory.getLogger(FileDownload.class);

	AbstractFileItem fileItem;
	File output;
	
	private long start = System.currentTimeMillis();
	
	public FileDownload(FileContainer fileContainer) throws Exception {
		StreamDescriptor stream = fileContainer.download();
		
		output = Files.createTempFile("down", stream.getFileName()).toFile();
		try (BufferedInputStream bi = new BufferedInputStream(stream.getStream()); FileOutputStream fo = new FileOutputStream(output)){
			byte[] buf = new byte[2046];			
			int read = -1;
			while ((read=bi.read(buf))!=-1) {
				fo.write(buf, 0, read);
			}
			fo.flush();
		}
		logger.info("download took {}", System.currentTimeMillis()-start);
		
		
		fileItem = fileContainer.get();
		logger.trace("FILE-DOWNLOAD with {} , {}", fileItem.getName(), fileItem.getContent().getSize());
		logger.trace("output name is {} ans length {}", output.getAbsolutePath(),output.length());

	}
	
		
	
	@Override
	public int read(Pointer buf, long size, long offset) {
		RandomAccessFile raf = null;
		int bytesToRead = (int) (size);
		byte[] mybuf = new byte[bytesToRead];
		int readTotal= 0;	
		
		try {
			raf = new RandomAccessFile(output, "r");
			raf.seek(offset);
			int read =0;
			logger.trace("{} BEFORE: bytes to read {} and read total {} and last read {}", fileItem.getName(), bytesToRead, readTotal, read);
			while ((read= raf.read(mybuf, 0 , bytesToRead-readTotal))!=-1 && bytesToRead>readTotal) {
				buf.put(readTotal, mybuf, 0, read);
				readTotal+= read;
				logger.trace("{} INSIDE: bytes to read {} and read total {} and last read {}", fileItem.getName() , bytesToRead, readTotal, read);
			}
			logger.trace("{} AFTER: bytes to read {} and read total {} and last read {}", fileItem.getName() , bytesToRead, readTotal, read);
		}catch (Exception e) {
			logger.error("error in read",e);
			return -ErrorCodes.ENOENT();
		} finally {
			try{ 
				if (raf!=null) raf.close();
			}catch (Exception e) {}
		}
		logger.trace("{} work finished!!! {}",fileItem.getName() , readTotal);
		return readTotal;	
	}




	public int flush() {
		logger.trace("called flush");
		output.delete();
		logger.info("file {} took {}", fileItem.getName(), System.currentTimeMillis()-start);
		return 0;
	}	


	public int getAttr(FileStat stat) {
		logger.trace("is in download");
		stat.st_mode.set(FileStat.S_IFREG | 0555);
		stat.st_size.set(fileItem.getContent().getSize());
		stat.st_mtim.tv_sec.set(fileItem.getLastModificationTime().toInstant().getEpochSecond());
		stat.st_ctim.tv_sec.set(fileItem.getLastModificationTime().toInstant().getEpochSecond());
		stat.st_atim.tv_sec.set(fileItem.getLastModificationTime().toInstant().getEpochSecond());
		//stat.st_birthtime.tv_sec.set(fileItem.getCreationTime().toInstant().getEpochSecond());
		return 0;
	}

	public File getOutputFile(){
		return output;
	}
	
}
