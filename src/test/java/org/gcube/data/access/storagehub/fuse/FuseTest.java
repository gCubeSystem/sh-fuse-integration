package org.gcube.data.access.storagehub.fuse;

import java.nio.file.Paths;
import java.util.Objects;

import org.gcube.data.access.storagehub.fs.StorageHubFS;
import org.junit.Test;

import jnr.ffi.Platform;

public class FuseTest {

	
	public void mount() {
		StorageHubFS memfs = new StorageHubFS("eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJSSklZNEpoNF9qdDdvNmREY0NlUDFfS1l0akcxVExXVW9oMkQ2Tzk1bFNBIn0","/gcube");
		try {
			String path;
            if (Objects.requireNonNull(Platform.getNativePlatform().getOS()) == Platform.OS.WINDOWS) {
                System.out.println("Im here");
                path = "J:\\";
            } else
                path = "/home/lucio/java/mnt";

			memfs.mount(Paths.get(path), true, true);
		} finally {
			memfs.umount();
		}
	}
	
}
