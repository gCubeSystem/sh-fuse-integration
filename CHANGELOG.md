This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data-access.sh-fuse-integration

## [v2.1.0-SNAPSHOT]

- porting to storagehub-client-2.0.0

## [v2.0.0] 2021-05-19
  
- porting to uma token [#21441]


## [v1.1.1] 2021-01-15
	
- issue on right displayed 
- [#20417] 

## [v1.1.0] 2020-09-07

- Solved issue on data download [#19651]
